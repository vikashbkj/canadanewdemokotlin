package com.canadanewdemo.model

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.canadanewdemo.network.AboutCanadaResponseModel
import com.canadanewdemo.network.ApiResponse
import org.hamcrest.CoreMatchers.*
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnit
import org.powermock.api.mockito.PowerMockito
import org.powermock.core.classloader.annotations.PrepareForTest
import org.powermock.modules.junit4.PowerMockRunner
import retrofit2.Call
import retrofit2.Response

@Suppress("UNCHECKED_CAST")
@RunWith(PowerMockRunner::class)
@PrepareForTest(Response::class, Log::class)
class CallbackImplTest {
    @Rule
    var mockitoRule = MockitoJUnit.rule()

    internal lateinit var SUT: CallbackImpl
    @Mock
    internal lateinit var mutableLiveData: MutableLiveData<ApiResponse>
    @Mock
    internal lateinit var apiResponse: ApiResponse

    @Before
    @Throws(Exception::class)
    fun setUp() {
        PowerMockito.mock(Response::class.java)
        PowerMockito.mockStatic(Log::class.java)
        SUT = CallbackImpl(mutableLiveData, apiResponse)
    }

    @Test
    fun onResponse_When_Success_Response_Passed_MutableLiveData_setValue_Method_Called() {
        val call = Mockito.mock(Call::class.java)
        val responseModel = AboutCanadaResponseModel()
        val response = Response.success(responseModel)

        SUT.onResponse(call as Call<AboutCanadaResponseModel>, response)

        assertThat(200, `is`(response.code()))
        assertThat(true, `is`(response.isSuccessful))
        assertThat(response.body(), instanceOf<Any>(AboutCanadaResponseModel::class.java))

        Mockito.verify(mutableLiveData).setValue(apiResponse)
    }

    @Test
    fun onResponse_When_Success_Response_Passed_As_Null_MutableLiveData_setValue_Called_Method_Called() {
        var responseModel: AboutCanadaResponseModel? = AboutCanadaResponseModel()
        responseModel = null
        val response = Response.success(responseModel)
        val call = Mockito.mock(Call::class.java)

        SUT.onResponse(call as Call<AboutCanadaResponseModel>, Response.success(responseModel))

        assertThat(200, `is`<Int>(response.code()))
        assertThat(true, `is`<Boolean>(response.isSuccessful))
        assertThat(response.body(), `is`<Any>(nullValue()))
        Mockito.verify<MutableLiveData<ApiResponse>>(mutableLiveData).setValue(apiResponse)
    }

    @Test
    fun onFailure_MutableLiveData_setValue_Method_Called() {
        val call = Mockito.mock(Call::class.java)
        val throwable = Exception("test exception")

        SUT.onFailure(call as Call<AboutCanadaResponseModel>, throwable)

        Mockito.verify<MutableLiveData<ApiResponse>>(mutableLiveData).setValue(apiResponse)
    }
}