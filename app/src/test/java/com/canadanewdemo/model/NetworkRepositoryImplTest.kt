package com.canadanewdemo.model

import androidx.lifecycle.MutableLiveData
import com.canadanewdemo.network.AboutCanadaResponseModel
import com.canadanewdemo.network.ApiResponse
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnit
import retrofit2.Call

@Suppress("UNCHECKED_CAST")
class NetworkRepositoryImplTest {
    @get:Rule
    var mockitoRule = MockitoJUnit.rule()

    lateinit var SUT: NetworkRepositoryImpl
    @Mock
    internal lateinit var mutableLiveData: MutableLiveData<ApiResponse>
    @Mock
    internal lateinit var apiResponse: ApiResponse

    @Before
    @Throws(Exception::class)
    fun setUp() {
        SUT = NetworkRepositoryImpl(mutableLiveData, apiResponse)
    }

    @Test
    fun callCanadaApi_Method_enqueue_Is_Called() {
//        Arrangement
        val call = Mockito.mock(Call::class.java)

//        Action
        SUT.callCanadaApi(call as Call<AboutCanadaResponseModel>)

//        Assert
        Mockito.verify<Call<AboutCanadaResponseModel>>(call as Call<AboutCanadaResponseModel>).enqueue(SUT)
    }
}