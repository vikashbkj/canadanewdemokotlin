package com.canadanewdemo.model

import com.canadanewdemo.TestUtils
import com.canadanewdemo.network.AboutCanadaResponseModel
import com.canadanewdemo.network.RemoteApiService
import com.canadanewdemo.utils.NetworkRepository
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnit

class CanadaModelImplTest {

    @get:Rule
    var mockitoRule = MockitoJUnit.rule()

    internal lateinit var SUT: CanadaModelImpl
    @Mock
    lateinit var remoteApiService: RemoteApiService
    @Mock
    internal lateinit var networkRepository: NetworkRepository<AboutCanadaResponseModel>

    @Before
    @Throws(Exception::class)
    fun setUp() {
        SUT = CanadaModelImpl(networkRepository, remoteApiService)
    }

    @Test
    fun fetchCanadaData_Calls_NetworkRepository_callCanadaApi_Method() {
        //        Arrangement
        val url = "canada_row_url"
        val call = TestUtils.getRetrofitInstance().create(RemoteApiService::class.java).callAboutCanadaApi(url)
        Mockito.`when`(remoteApiService.callAboutCanadaApi(url)).thenReturn(call)

        //        Action
        SUT.fetchCanadaData(url)

        //        Assert
        Mockito.verify(networkRepository).callCanadaApi(call)
    }
}