package com.canadanewdemo.viewmodel

import android.text.TextUtils
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.canadanewdemo.network.AboutCanadaResponseModel
import com.canadanewdemo.network.ApiResponse
import com.canadanewdemo.network.Row
import com.canadanewdemo.utils.CanadaMVP
import org.hamcrest.CoreMatchers.`is`
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Matchers.anyString
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnit
import org.powermock.api.mockito.PowerMockito
import org.powermock.core.classloader.annotations.PrepareForTest
import org.powermock.modules.junit4.PowerMockRunner
import java.util.ArrayList

@RunWith(PowerMockRunner::class)
@PrepareForTest(Log::class, TextUtils::class)
class AboutCanadaViewModelTest {
    @Rule
    var mockitoRule = MockitoJUnit.rule()

    lateinit var SUT: AboutCanadaViewModel
    @Mock
    internal lateinit var model: CanadaMVP.Model
    @Mock
    internal lateinit var mutableLiveData: MutableLiveData<ApiResponse>

    @Before
    @Throws(Exception::class)
    fun setUp() {
        PowerMockito.mockStatic(Log::class.java)
        PowerMockito.mockStatic(TextUtils::class.java)
        SUT = AboutCanadaViewModel(model, mutableLiveData)
    }

    @Test
    fun callCanadaApi_When_Returns_Non_EmptyList_Api_Not_Called() {
        //        Arrangement
        val aboutCanadaResponseModel = AboutCanadaResponseModel()
        val rowList = ArrayList<Row>()
        rowList.add(Row())
        aboutCanadaResponseModel.rows = rowList
        val apiResponse = ApiResponse()
        apiResponse.responseModel = aboutCanadaResponseModel
        Mockito.`when`<Any>(mutableLiveData.getValue()).thenReturn(apiResponse)

        val url = "canada_url"

        //        Action
        SUT.callCanadaApi(url)

        //        Assert
        Mockito.verify(model, Mockito.never()).fetchCanadaData(url)
    }

    @Test
    fun callCanadaApi_When_Returns_EmptyList_Api_Called() {
        //        Arrangement
        val rowList = ArrayList<Row>()
        val apiResponse = ApiResponse()
        val aboutCanadaResponseModel = AboutCanadaResponseModel()
        aboutCanadaResponseModel.rows = rowList
        apiResponse.responseModel = aboutCanadaResponseModel

        Mockito.`when`<Any>(mutableLiveData.getValue()).thenReturn(apiResponse)
        val url = "canada_url"

        //        Action
        SUT.callCanadaApi(url)

        //        Assert
        Mockito.verify(model).fetchCanadaData(url)
    }

    @Test
    fun getRowList_Returns_EmptyList_When_Response_Is_Not_Available() {
        //        Arrangement
        val tempRowList = ArrayList<Row>()
        val apiResponse = ApiResponse()
        val aboutCanadaResponseModel = AboutCanadaResponseModel()
        aboutCanadaResponseModel.rows = tempRowList
        apiResponse.responseModel = aboutCanadaResponseModel

        //        Action
        val rowList = SUT.getRowList()

        //        Assert
        assertThat(true, `is`<Boolean>(rowList.isEmpty()))
    }

    @Test
    fun getRowList_Returns_Value_When_Response_Is_Available() {
        //        Arrangement
        val aboutCanadaResponseModel = AboutCanadaResponseModel()
        val tempRowList = ArrayList<Row>()
        tempRowList.add(Row())
        aboutCanadaResponseModel.rows = tempRowList
        val apiResponse = ApiResponse()
        apiResponse.responseModel = aboutCanadaResponseModel
        Mockito.`when`<Any>(mutableLiveData.getValue()).thenReturn(apiResponse)

        //        Action
        val rowList = SUT.getRowList()

        //        Assert
        assertThat(false, `is`<Boolean>(rowList.isEmpty()))
    }

    @Test
    fun onBindViewHolder_When_Row_Is_Null_RowItemView_No_Method_Called() {
        val apiResponse = ApiResponse()
        val responseModel = AboutCanadaResponseModel()
        val rowList = ArrayList<Row>()
        responseModel.rows = rowList
        apiResponse.responseModel = responseModel
        Mockito.`when`<Any>(mutableLiveData.getValue()).thenReturn(apiResponse)

        val rowItemView = Mockito.mock(CanadaMVP.RowItemView::class.java)
        val position = 0

        SUT.onBindViewHolder(rowItemView, position)

        Mockito.verify(rowItemView, Mockito.never()).setTitle(anyString())
        Mockito.verify(rowItemView, Mockito.never()).setDescription(anyString())
        Mockito.verify(rowItemView, Mockito.never()).setImage(anyString())
    }

    @Test
    fun onBindViewHolder_When_Row_Title_Is_Empty_RowItemView_setTitle_Method_Not_Called() {
        val apiResponse = ApiResponse()
        val responseModel = AboutCanadaResponseModel()
        val rowList = ArrayList<Row>()
        val row = Row()
        rowList.add(row)
        responseModel.rows = rowList
        apiResponse.responseModel = responseModel
        Mockito.`when`<Any>(mutableLiveData.getValue()).thenReturn(apiResponse)

        val rowItemView = Mockito.mock(CanadaMVP.RowItemView::class.java)
        val position = 0

        SUT.onBindViewHolder(rowItemView, position)

        Mockito.verify(rowItemView, Mockito.never()).setTitle(anyString())
    }

    @Test
    fun onBindViewHolder_When_Row_Title_Is_Not_Empty_RowItemView_setTitle_Method_Called() {
        val apiResponse = ApiResponse()
        val responseModel = AboutCanadaResponseModel()
        val rowList = ArrayList<Row>()
        val row = Row()
        row.setTitle("title")
        rowList.add(row)
        responseModel.rows = rowList
        apiResponse.responseModel = responseModel
        Mockito.`when`<Any>(mutableLiveData.getValue()).thenReturn(apiResponse)

        val rowItemView = Mockito.mock(CanadaMVP.RowItemView::class.java)
        val position = 0

        SUT.onBindViewHolder(rowItemView, position)

        Mockito.verify(rowItemView).setTitle(anyString())
    }

    @Test
    fun onBindViewHolder_When_Row_Description_Is_Empty_RowItemView_setDescription_Method_Called() {
        val apiResponse = ApiResponse()
        val responseModel = AboutCanadaResponseModel()
        val rowList = ArrayList<Row>()
        val row = Row()
        rowList.add(row)
        responseModel.rows = rowList
        apiResponse.responseModel = responseModel
        Mockito.`when`<Any>(mutableLiveData.getValue()).thenReturn(apiResponse)

        val rowItemView = Mockito.mock(CanadaMVP.RowItemView::class.java)
        val position = 0

        SUT.onBindViewHolder(rowItemView, position)

        Mockito.verify(rowItemView).setDescription(anyString())
    }

    @Test
    fun onBindViewHolder_When_Row_Description_Is_Not_Empty_RowItemView_setDescription_Method_Called() {
        val apiResponse = ApiResponse()
        val responseModel = AboutCanadaResponseModel()
        val rowList = ArrayList<Row>()
        val row = Row()
        row.setDescription("description")
        rowList.add(row)
        responseModel.rows = rowList
        apiResponse.responseModel = responseModel
        Mockito.`when`<Any>(mutableLiveData.getValue()).thenReturn(apiResponse)

        val rowItemView = Mockito.mock(CanadaMVP.RowItemView::class.java)
        val position = 0

        SUT.onBindViewHolder(rowItemView, position)

        Mockito.verify(rowItemView).setDescription(anyString())
    }

    @Test
    fun onBindViewHolder_When_Row_Image_Is_Empty_RowItemView_setImage_Method_Not_Called() {
        val apiResponse = ApiResponse()
        val responseModel = AboutCanadaResponseModel()
        val rowList = ArrayList<Row>()
        val row = Row()
        rowList.add(row)
        responseModel.rows = rowList
        apiResponse.responseModel = responseModel
        Mockito.`when`<Any>(mutableLiveData.getValue()).thenReturn(apiResponse)

        val rowItemView = Mockito.mock(CanadaMVP.RowItemView::class.java)
        val position = 0

        SUT.onBindViewHolder(rowItemView, position)

        Mockito.verify(rowItemView, Mockito.never()).setImage(anyString())
    }

    @Test
    fun onBindViewHolder_When_Row_Image_Is_Not_Empty_RowItemView_setImage_Method_Called() {
        val apiResponse = ApiResponse()
        val responseModel = AboutCanadaResponseModel()
        val rowList = ArrayList<Row>()
        val row = Row()
        row.setImageHref("image")
        rowList.add(row)
        responseModel.rows = rowList
        apiResponse.responseModel = responseModel
        Mockito.`when`<Any>(mutableLiveData.getValue()).thenReturn(apiResponse)

        val rowItemView = Mockito.mock(CanadaMVP.RowItemView::class.java)
        val position = 0

        SUT.onBindViewHolder(rowItemView, position)

        Mockito.verify(rowItemView).setImage(anyString())
    }
}