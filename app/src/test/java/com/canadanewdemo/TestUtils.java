package com.canadanewdemo;

import com.canadanewdemo.utils.AppConstants;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.GsonBuilder;
import com.google.gson.LongSerializationPolicy;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import okio.BufferedSource;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.util.concurrent.TimeUnit;

public class TestUtils {
    public static Retrofit getRetrofitInstance() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .writeTimeout(AppConstants.Constants.getWRITE_TIMEOUT(), TimeUnit.SECONDS)
                .readTimeout(AppConstants.Constants.getREAD_TIMEOUT(), TimeUnit.SECONDS)
                .connectTimeout(AppConstants.Constants.getCONNECTION_TIMEOUT(), TimeUnit.SECONDS)
                .addInterceptor(loggingInterceptor)
                .build();

        GsonBuilder gsonBuilder = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)
                .setLongSerializationPolicy(LongSerializationPolicy.STRING)
                .serializeNulls()
                .setPrettyPrinting()
                .enableComplexMapKeySerialization()
                .setFieldNamingStrategy(FieldNamingPolicy.IDENTITY);

        GsonConverterFactory gsonConverterFactory = GsonConverterFactory.create(gsonBuilder.create());

        Retrofit retrofit = new Retrofit.Builder()
                .client(okHttpClient)
                .addConverterFactory(gsonConverterFactory)
//                .baseUrl(mockWebServer.url(AppConstants.SERVER_BASE_URL))
                .baseUrl(AppConstants.Constants.getSERVER_BASE_URL())
//                .callbackExecutor(Executors.newFixedThreadPool(1))
                .build();

        return retrofit;
    }

    public static ResponseBody createCustomTestResponseBody() {
        ResponseBody responseBody = new ResponseBody() {

            @Override
            public MediaType contentType() {
                return null;
            }

            @Override
            public long contentLength() {
                return 0;
            }

            @Override
            public BufferedSource source() {
                return null;
            }
        };
        return responseBody;
    }
}