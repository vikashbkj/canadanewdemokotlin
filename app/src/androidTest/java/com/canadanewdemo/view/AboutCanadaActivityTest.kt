package com.canadanewdemo.view

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.RootMatchers
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.rule.ActivityTestRule
import org.hamcrest.Matchers
import org.junit.Assert.*
import org.junit.FixMethodOrder
import org.junit.Rule
import org.junit.Test
import org.junit.runners.MethodSorters
import androidx.test.espresso.Espresso.onView
import android.widget.TextView
import org.junit.Before


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class AboutCanadaActivityTest {

    @get:Rule
    var activityTestRule = ActivityTestRule<AboutCanadaActivity>(AboutCanadaActivity::class.java)
    var canadaRecycler : RecyclerView ?= null

    @Before
    fun setUp() {
        canadaRecycler = activityTestRule.getActivity().findViewById<RecyclerView>(com.canadanewdemo.R.id.canadaRecycler)
        try {
            Thread.sleep(5000)
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
    }

    @Test
    fun test_First_RecyclerView_Shown(){
        Espresso.onView(ViewMatchers.withId(com.canadanewdemo.R.id.canadaRecycler))
            .inRoot(
                RootMatchers.withDecorView(
                    Matchers.`is`(activityTestRule.getActivity().getWindow().getDecorView())
                )
            ).check(matches(isDisplayed()))
    }

//    Testcase for #RecyclerView scroll to bottom
    @Test
    fun test_RecyclerView_Scroll() {
        val itemCount = canadaRecycler?.getAdapter()?.getItemCount()
        if (itemCount != null) {
            onView(ViewMatchers.withId(com.canadanewdemo.R.id.canadaRecycler)).perform(RecyclerViewActions.scrollToPosition<CanadaVH>( itemCount -1)).check(matches(isDisplayed()))
            Thread.sleep(2000)
        }
    }

    @Test
    fun test_Last_Item_Title_In_RecyclerView_Matches(){
        val itemCount = canadaRecycler?.getAdapter()?.getItemCount()
        if (itemCount != null) {
            onView(ViewMatchers.withId(com.canadanewdemo.R.id.canadaRecycler)).perform(RecyclerViewActions.scrollToPosition<CanadaVH>( itemCount -1)).check(matches(isDisplayed()))
            val canadaVH: RecyclerView.ViewHolder? = canadaRecycler?.findViewHolderForAdapterPosition(itemCount -1)
            assertEquals("Language", canadaVH?.let { canadaVH.itemView.findViewById<TextView>(com.canadanewdemo.R.id.titleTxt).text.toString() })
        }
    }

    @Test
    fun test_Last_Item_Description_In_RecyclerView_Does_Not_Match(){
        val itemCount = canadaRecycler?.getAdapter()?.getItemCount()
        if (itemCount != null) {
            onView(ViewMatchers.withId(com.canadanewdemo.R.id.canadaRecycler)).perform(RecyclerViewActions.scrollToPosition<CanadaVH>( itemCount -1)).check(matches(isDisplayed()))
            val canadaVH: CanadaVH ?= canadaRecycler?.findViewHolderForAdapterPosition(itemCount -1) as CanadaVH
            assertNotEquals("Flag", canadaVH?.let { canadaVH.itemView.findViewById<TextView>(com.canadanewdemo.R.id.descriptionTxt).text.toString() } )
        }
    }
}