package com.canadanewdemo.dagger2

import android.content.Context
import com.canadanewdemo.view.AboutCanadaActivity
import com.canadanewdemo.view.FragmentCanada
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = [ModuleNetwork::class, ModuleContext::class, CanadaModule::class])
interface AppComponent {
    fun injectFragmentCanada(fragmentCanada: FragmentCanada)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun setContext(context: Context): Builder

        @BindsInstance
        fun setBaseUrl(baseUrl: String): Builder

        @BindsInstance
        fun setValidateRetrofitEagerly(validateRetrofitEagerly: Boolean?): Builder

        fun build(): AppComponent
    }
}