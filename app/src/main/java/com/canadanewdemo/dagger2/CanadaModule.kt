package com.canadanewdemo.dagger2

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.canadanewdemo.model.CanadaModelImpl
import com.canadanewdemo.model.NetworkRepositoryImpl
import com.canadanewdemo.network.AboutCanadaResponseModel
import com.canadanewdemo.network.ApiResponse
import com.canadanewdemo.network.RemoteApiService
import com.canadanewdemo.utils.CanadaMVP
import com.canadanewdemo.viewmodel.CustomViewModelFactoryImpl
import com.canadanewdemo.utils.NetworkRepository
import com.canadanewdemo.viewmodel.AboutCanadaViewModel
import dagger.MapKey
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import javax.inject.Provider
import javax.inject.Singleton
import kotlin.reflect.KClass

@Module
class CanadaModule {
    private val TAG = "CanadaModule"

    @MustBeDocumented
    @Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER)
    @kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
    @MapKey
    internal annotation class ViewModelKey(val value: KClass<out ViewModel>)


    @Provides
    @IntoMap
    @ViewModelKey(AboutCanadaViewModel::class)
    fun provideAboutCanadaViewModel(model: CanadaMVP.Model, mutableLiveData: MutableLiveData<ApiResponse>): ViewModel {
        return AboutCanadaViewModel(model, mutableLiveData)
    }

    @Provides
    fun provideCustomViewModelFactoryImpl(providerMap: MutableMap<Class<out ViewModel>, Provider<ViewModel>>): CustomViewModelFactoryImpl {
        Log.e(TAG, "provideCustomViewModelFactoryImpl: ")
        return CustomViewModelFactoryImpl(providerMap)
    }

    @Provides
    fun provideCanadaModel(
        networkRepository: NetworkRepository<AboutCanadaResponseModel>,
        remoteApiService: RemoteApiService
    ): CanadaMVP.Model {
        return CanadaModelImpl(networkRepository, remoteApiService)
    }

    @Provides
    fun provideAboutCanadaResponseModel(
        mutableLiveData: MutableLiveData<ApiResponse>,
        apiResponse: ApiResponse
    ): NetworkRepository<AboutCanadaResponseModel> {
        return NetworkRepositoryImpl(mutableLiveData, apiResponse)
    }

    @Provides
    fun provideApiResponse(): ApiResponse {
        return ApiResponse()
    }

    @Singleton
    @Provides
    fun provideMutableLiveData(): MutableLiveData<ApiResponse> {
        return MutableLiveData()
    }
}