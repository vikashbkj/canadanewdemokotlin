package com.canadanewdemo.dagger2

import android.content.Context
import dagger.Module
import dagger.Provides

@Module
class ModuleContext {
    @Provides
    fun provideContext(context: Context): Context {
        return context
    }
}