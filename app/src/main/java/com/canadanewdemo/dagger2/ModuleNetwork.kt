package com.canadanewdemo.dagger2

import com.canadanewdemo.BuildConfig
import com.canadanewdemo.network.RemoteApiService
import com.canadanewdemo.utils.AppConstants
import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import com.google.gson.LongSerializationPolicy
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Named

@Module
class ModuleNetwork {

    @Named("retrofit_validate_eagerly")
    @Provides
    fun provideBooleanValidateEagerly(isValidateEagerly: Boolean?): Boolean? {
        return isValidateEagerly
    }

    @Provides
    fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        val loggingInterceptor = HttpLoggingInterceptor()
        if (BuildConfig.DEBUG) {
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BASIC
        } else {
            loggingInterceptor.level = HttpLoggingInterceptor.Level.NONE
        }
        return loggingInterceptor
    }


    @Provides
    fun provideOkHttpClient(loggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
        val builder = OkHttpClient.Builder()
            .connectTimeout(AppConstants.CONNECTION_TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(AppConstants.READ_TIMEOUT, TimeUnit.SECONDS)
            .writeTimeout(AppConstants.WRITE_TIMEOUT, TimeUnit.SECONDS)
            .addInterceptor(loggingInterceptor)

        return builder.build()
    }

    @Provides
    fun provideGsonBuilder(): GsonBuilder {
        return GsonBuilder()
            .setFieldNamingStrategy(FieldNamingPolicy.IDENTITY)
            .setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)
            .setLongSerializationPolicy(LongSerializationPolicy.STRING)
            .setPrettyPrinting()
            .enableComplexMapKeySerialization()
            .serializeSpecialFloatingPointValues()
            .serializeNulls()
    }

    @Provides
    fun provideGsonConverterFactory(gsonBuilder: GsonBuilder): GsonConverterFactory {
        return GsonConverterFactory.create(gsonBuilder.create())
    }

    @Provides
    fun provideRetrofit(
        baseUrl: String, okHttpClient: OkHttpClient, gsonConverterFactory: GsonConverterFactory, @Named("retrofit_validate_eagerly") isValidateEagerly: Boolean?
    ): Retrofit {
        val builder = Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(okHttpClient)
            .addConverterFactory(gsonConverterFactory)
            if (isValidateEagerly != null){
                builder.validateEagerly(isValidateEagerly)
            }

        return builder.build()
    }

    @Provides
    fun provideRemoteApiService(retrofit: Retrofit): RemoteApiService {
        return retrofit.create<RemoteApiService>(RemoteApiService::class.java)
    }
}
