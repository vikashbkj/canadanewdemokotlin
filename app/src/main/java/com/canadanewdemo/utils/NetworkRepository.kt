package com.canadanewdemo.utils

import retrofit2.Call

interface NetworkRepository<T> {
    fun callCanadaApi(call: Call<T>)
}