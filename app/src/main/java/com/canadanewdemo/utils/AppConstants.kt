package com.canadanewdemo.utils

class AppConstants {
    companion object Constants {
        const val WRITE_TIMEOUT = 30L
        const val READ_TIMEOUT = 20L
        const val CONNECTION_TIMEOUT = 30L
        const val SERVER_BASE_URL = "https://dl.dropboxusercontent.com/"
        const val API_URL = "https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json"

        const val LOADING = "LOADING"
        const val SUCCESS = "SUCCESS"
        const val ERROR = "ERROR"

        const val ABOUT_CANADA_VIEW_MODEL = "ABOUT_CANADA_VIEW_MODEL"
    }
}