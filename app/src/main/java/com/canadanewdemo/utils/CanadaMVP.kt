package com.canadanewdemo.utils

import androidx.lifecycle.MutableLiveData
import com.canadanewdemo.network.ApiResponse
import com.canadanewdemo.network.Row

interface CanadaMVP {

    interface RowItemView {
        fun setTitle(title: String)
        fun setDescription(description: String)
        fun setImage(image: String)
    }

    interface ViewModelPresenter<T> {
        fun getRowList(): ArrayList<Row>
        fun callCanadaApi(url: String): MutableLiveData<ApiResponse>?
        fun onBindViewHolder(rowItemView: RowItemView, position: Int)
        fun setSwipeRefreshStatus()
    }

    interface Model {
        fun fetchCanadaData(url: String)
    }
}
