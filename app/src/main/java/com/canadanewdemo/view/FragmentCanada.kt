package com.canadanewdemo.view

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.canadanewdemo.R
import com.canadanewdemo.databinding.FragmentCanadaBinding
import com.canadanewdemo.network.ApiResponse
import com.canadanewdemo.utils.AppConstants
import com.canadanewdemo.viewmodel.AboutCanadaViewModel
import com.canadanewdemo.viewmodel.CustomViewModelFactoryImpl
import javax.inject.Inject

class FragmentCanada : Fragment(), SwipeRefreshLayout.OnRefreshListener {
    private lateinit var binding: FragmentCanadaBinding
    private lateinit var activity: AboutCanadaActivity

    @Inject
    lateinit var factory: CustomViewModelFactoryImpl
    private var viewModel: AboutCanadaViewModel? = null
    private var adapter: CanadaAdapter? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is AboutCanadaActivity){
            activity = context;
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate<FragmentCanadaBinding>(inflater, R.layout.fragment_canada, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        activity.getAppComponent()?.injectFragmentCanada(this)
        viewModel = ViewModelProvider(this, factory).get(AppConstants.ABOUT_CANADA_VIEW_MODEL, AboutCanadaViewModel::class.java)

        binding?.swipeRefresh?.setOnRefreshListener(this)
        adapter = CanadaAdapter(viewModel)
        binding?.canadaRecycler?.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false) as RecyclerView.LayoutManager?
        binding?.canadaRecycler?.addItemDecoration(DividerItemDecoration(activity, RecyclerView.VERTICAL))
        binding?.canadaRecycler?.adapter = adapter

        makeApiCall(viewModel);
    }

    private fun makeApiCall(viewModel: AboutCanadaViewModel?) {
        viewModel?.callCanadaApi(AppConstants.API_URL)?.observe(this,
            Observer<ApiResponse> { apiResponse ->
                binding?.swipeRefresh?.isRefreshing = false;
                when (apiResponse.status) {
                    AppConstants.SUCCESS -> {
                        binding?.errorFL?.visibility = View.GONE
                        binding?.progressBar?.visibility = View.INVISIBLE
                        adapter?.notifyDataSetChanged()
                    }

                    AppConstants.ERROR -> {
                        binding?.progressBar?.visibility = View.INVISIBLE
                        binding?.errorFL?.visibility = View.VISIBLE
                    }

                    AppConstants.LOADING -> {
                        binding?.progressBar?.visibility = View.VISIBLE
                        binding?.errorFL?.visibility = View.GONE
                    }
                }
            })

    }

    override fun onRefresh() {
        binding?.swipeRefresh?.isRefreshing = true;
        viewModel?.setSwipeRefreshStatus()
        makeApiCall(viewModel)
    }
}