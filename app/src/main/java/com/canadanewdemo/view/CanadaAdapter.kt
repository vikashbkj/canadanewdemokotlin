package com.canadanewdemo.view

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.canadanewdemo.R
import com.canadanewdemo.databinding.CanadaVhBinding
import com.canadanewdemo.viewmodel.AboutCanadaViewModel

class CanadaAdapter(private val viewModel: AboutCanadaViewModel?) : RecyclerView.Adapter<CanadaVH>() {
    private var context: Context? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CanadaVH {
        this.context = parent.context
        val canadaVhBinding =
            DataBindingUtil.inflate<ViewDataBinding>(LayoutInflater.from(context), R.layout.canada_vh, parent, false)
        return CanadaVH(canadaVhBinding as CanadaVhBinding, this)
    }

    override fun onBindViewHolder(holder: CanadaVH, position: Int) {
        viewModel?.onBindViewHolder(holder, position)
    }

    override fun getItemCount(): Int {
        val rowList = viewModel?.getRowList()
        if (rowList?.size != null){
            return rowList.size
        }
        return 0
    }
}