package com.canadanewdemo.view

import android.util.Log
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.canadanewdemo.R
import com.canadanewdemo.databinding.CanadaVhBinding
import com.canadanewdemo.utils.CanadaMVP


class CanadaVH(private val canadaVhBinding: CanadaVhBinding, private val canadaAdapter: CanadaAdapter) :
    RecyclerView.ViewHolder(canadaVhBinding.getRoot()), CanadaMVP.RowItemView {
    private val TAG = "CanadaVH -->>"
    override fun setTitle(title: String) {
        canadaVhBinding.titleTxt.setText(title)
    }

    override fun setDescription(description: String) {
        Log.e(TAG, "setDescription :: description :: "+ description + " :: adapterPosition :: " + adapterPosition)
        canadaVhBinding.descriptionTxt.setText(description)
    }

    override fun setImage(image: String) {
        Glide.with(canadaVhBinding.getRoot().getContext())
            .load(image)
            .placeholder(R.drawable.ic_launcher_background)
            .into(canadaVhBinding.rowImg)
    }

}