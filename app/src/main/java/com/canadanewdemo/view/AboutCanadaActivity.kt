package com.canadanewdemo.view

import android.os.Bundle
import androidx.fragment.app.FragmentTransaction
import com.canadanewdemo.BaseActivity
import com.canadanewdemo.R

@Suppress("UNREACHABLE_CODE")
class AboutCanadaActivity : BaseActivity() {
    private val TAG = "AboutCanadaActivity-->"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       setContentView(R.layout.activity_about_canada)

        val frgmentTransaction: FragmentTransaction = supportFragmentManager.beginTransaction()
        frgmentTransaction.add(R.id.mainCL, FragmentCanada())
        frgmentTransaction.commit()
    }
}
