package com.canadanewdemo

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.canadanewdemo.dagger2.AppComponent

abstract class BaseActivity : AppCompatActivity() {
    private var appComponent: AppComponent? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        appComponent = (application as CustomApplication).getAppComponent()
    }

    fun getAppComponent(): AppComponent? {
        return appComponent
    }
}