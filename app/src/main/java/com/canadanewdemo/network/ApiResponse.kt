package com.canadanewdemo.network

import javax.inject.Inject


open class ApiResponse {
    var status: String? = null
    var url: String? = null
    var error: Throwable? = null
    var errorCode: Int = 0
    var responseModel: AboutCanadaResponseModel? = null

    @Inject
    constructor() {
    }

    constructor(status: String) {
        this.status = status
    }

    fun setEmptyBody() {
        this.responseModel = null
    }

}