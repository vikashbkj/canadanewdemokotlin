package com.canadanewdemo.network

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Url

interface RemoteApiService {
    //https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json
    @GET
    fun callAboutCanadaApi(@Url url: String): Call<AboutCanadaResponseModel>
}