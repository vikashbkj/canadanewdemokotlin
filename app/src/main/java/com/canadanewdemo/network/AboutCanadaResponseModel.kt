package com.canadanewdemo.network

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class AboutCanadaResponseModel {

    @SerializedName("title")
    @Expose
    var title: String? = null
    @SerializedName("rows")
    @Expose
    var rows: List<Row>? = null

}