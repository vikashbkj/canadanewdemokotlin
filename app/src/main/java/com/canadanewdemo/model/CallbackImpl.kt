package com.canadanewdemo.model

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.canadanewdemo.network.AboutCanadaResponseModel
import com.canadanewdemo.network.ApiResponse
import com.canadanewdemo.utils.AppConstants
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

open class CallbackImpl(private val mutableLiveData: MutableLiveData<ApiResponse>, private val apiResponse: ApiResponse) :
    Callback<AboutCanadaResponseModel> {

    private val TAG = "CallbackImpl-->"

    override fun onResponse(call: Call<AboutCanadaResponseModel>, response: Response<AboutCanadaResponseModel>) {
        Log.e(TAG, "onResponse: mutableLiveData:: $mutableLiveData")
        if (response.code() == 200 && response.isSuccessful() && response.body() != null) {
            apiResponse.status = AppConstants.SUCCESS
            apiResponse.responseModel = response.body()
        } else {
            apiResponse.errorCode = response.code()
            apiResponse.setEmptyBody()
        }
        mutableLiveData.setValue(apiResponse)
    }

    override fun onFailure(call: Call<AboutCanadaResponseModel>, t: Throwable) {
        Log.e(TAG, "onFailure: mutableLiveData:: $mutableLiveData")
        apiResponse.error = t
        apiResponse.status = AppConstants.ERROR
        mutableLiveData.setValue(apiResponse)
    }
}