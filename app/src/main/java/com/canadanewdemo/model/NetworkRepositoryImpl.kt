package com.canadanewdemo.model

import androidx.lifecycle.MutableLiveData
import com.canadanewdemo.network.AboutCanadaResponseModel
import com.canadanewdemo.network.ApiResponse
import com.canadanewdemo.utils.NetworkRepository
import retrofit2.Call

open class NetworkRepositoryImpl(mutableLiveData: MutableLiveData<ApiResponse>, apiResponse: ApiResponse) :
    CallbackImpl(mutableLiveData, apiResponse), NetworkRepository<AboutCanadaResponseModel> {

    override fun callCanadaApi(call: Call<AboutCanadaResponseModel>) {
        call.enqueue(this)
    }
}