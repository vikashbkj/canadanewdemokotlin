package com.canadanewdemo.model

import com.canadanewdemo.network.AboutCanadaResponseModel
import com.canadanewdemo.network.RemoteApiService
import com.canadanewdemo.utils.CanadaMVP
import com.canadanewdemo.utils.NetworkRepository

class CanadaModelImpl(
    private val networkRepository: NetworkRepository<AboutCanadaResponseModel>,
    private val remoteApiService: RemoteApiService) : CanadaMVP.Model {

    override fun fetchCanadaData(url: String) {
     networkRepository.callCanadaApi(remoteApiService.callAboutCanadaApi(url))
    }
}