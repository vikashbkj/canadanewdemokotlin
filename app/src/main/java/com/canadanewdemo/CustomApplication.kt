package com.canadanewdemo

import android.app.Application
import com.canadanewdemo.dagger2.AppComponent
import com.canadanewdemo.dagger2.DaggerAppComponent
import com.canadanewdemo.utils.AppConstants

class CustomApplication: Application() {
    private var appComponent: AppComponent? = null

    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent.builder()
            .setContext(this)
            .setBaseUrl(AppConstants.SERVER_BASE_URL)
            .setValidateRetrofitEagerly(true)
            .build()
    }

    fun getAppComponent(): AppComponent? {
        return appComponent
    }
}