package com.canadanewdemo.viewmodel

import android.text.TextUtils
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.canadanewdemo.network.ApiResponse
import com.canadanewdemo.utils.AppConstants
import com.canadanewdemo.utils.CanadaMVP
import kotlin.collections.ArrayList
import com.canadanewdemo.network.Row as Row1

class AboutCanadaViewModel(
    private val model: CanadaMVP.Model?,
    private val mutableLiveData: MutableLiveData<ApiResponse>?
) : ViewModel(), CanadaMVP.ViewModelPresenter<ApiResponse> {

    private val TAG = "AboutCanadaViewM-->"

    override fun getRowList(): kotlin.collections.ArrayList<Row1> {
        val apiResponse = mutableLiveData?.value
        if (apiResponse != null && apiResponse.responseModel != null) {
            val rowList = kotlin.collections.ArrayList<Row1>(apiResponse.responseModel?.rows!!.size)
            for (i in 0 until apiResponse.responseModel?.rows!!.size) {
                val row = apiResponse.responseModel?.rows?.get(i)
                if (!(TextUtils.isEmpty(row?.getTitle()) && TextUtils.isEmpty(row?.getDescription()) && TextUtils.isEmpty(row?.getImageHref()))) {
                    row?.let { rowList.add(it) }
                }
            }
            Log.e(TAG, "getRowList: rowList.size() :: " + rowList.size)
            return rowList
        }
        return ArrayList()
    }

    override fun callCanadaApi(url: String): MutableLiveData<ApiResponse>? {
        Log.e(TAG, "callCanadaApi: mutableLiveData :: $mutableLiveData")

        if (getRowList().isEmpty()) {
            val apiResponse = ApiResponse(AppConstants.LOADING)
            if (mutableLiveData != null) {
                mutableLiveData.setValue(apiResponse)
            }
            model?.fetchCanadaData(url)
        }
        
        return mutableLiveData
    }

    override fun onBindViewHolder(rowItemView: CanadaMVP.RowItemView, position: Int) {
        Log.e(TAG, "onBindViewHolder: position :: $position")
        if (!getRowList().isEmpty()) {
            val row = getRowList()[position]
            if (row != null) {
                if (row.getTitle() != null) {
                    rowItemView.setTitle(row.getTitle().toString().trim())
                }
                if (row.getDescription() != null) {
                    rowItemView.setDescription(row.getDescription().toString().trim())
                } else {
                    rowItemView.setDescription("")
                }
                if (row.getImageHref() != null) {
                    rowItemView.setImage(row.getImageHref().toString().trim())
                }
            }
        }
    }

    override fun setSwipeRefreshStatus() {
        mutableLiveData?.setValue(ApiResponse(AppConstants.LOADING));
    }
}